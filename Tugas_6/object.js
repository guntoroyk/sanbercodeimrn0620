// no 1
function arrayToObject(arr) {
  // Code di sini
  var peopleObject = {};
  for (let i = 0; i < arr.length; i++) {
    var thisYear = new Date().getFullYear();
    var birthYear = arr[i][3];
    var age =
      birthYear && birthYear < thisYear
        ? thisYear - birthYear
        : "Invalid Birth Year";
    var obj = {
      firstName: arr[i][0],
      lastName: arr[i][1],
      gender: arr[i][2],
      age,
    };
    const key = `${i + 1}. ${arr[i][0]} ${arr[i][1]}`;
    peopleObject[key] = obj;
  }
  console.log(peopleObject);
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

// no 2
function shoppingTime(memberId, money) {
  // you can only write your code here!
    var listProduct = [
        {
            name: 'Sepatu Stacattu',
            price: 1500000
        },
        {
            name: 'Baju Zoro',
            price: 500000
        },
        {
            name: 'Baju H&N',
            price: 250000
        },
        {
            name: 'Sweater Uniklooh',
            price: 175000
        },
        {
            name: 'Casing Handphone',
            price: 50000
        },
    ]
    var result = {};
    var listPurchased = []
    var changeMoney = money;

    if (!memberId) return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    if (money < 50000) return 'Mohon maaf, uang tidak cukup';

    listProduct.forEach(product => {
        if (changeMoney >= product.price) {
            listPurchased.push(product.name);
            changeMoney -= product.price;
        }
    })

    result.memberId = memberId;
    result.money = money;
    result.listPurchased = listPurchased;
    result.changeMoney = changeMoney;

    return result;
}

// TEST CASES
console.log()
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// no 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here

    let result = [];
    for (let i = 0; i < arrPenumpang.length; i++) {
        let startIndex = rute.indexOf(arrPenumpang[i][1]);
        let endIndex = rute.indexOf(arrPenumpang[i][2]);
        let bayar = Math.abs(startIndex - endIndex) * 2000
        
        let obj = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar
        };
        result.push(obj);
    }
    return result;
}
   
  //TEST CASE
console.log();
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]