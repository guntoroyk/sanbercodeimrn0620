import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/About';
import TodoApp from './Tugas/Tugas14/App';
import Quiz3 from './Tugas/Quiz_3/index';

export default function App() {
  return (
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
