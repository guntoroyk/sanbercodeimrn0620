// If Else
var nama = "John";
var peran = "Warewolf";

if (!nama) {
  console.log("Nama harus diisi!");
} else if (nama && !peran) {
  console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else if (nama && peran) {
  console.log(`Selamat datang di Dunia Warewolf, ${nama}`);

  if (peran === "Penyihir") {
    console.log(
      `Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi warewolf!`
    );
  } else if (peran === "Guard") {
    console.log(
      `Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`
    );
  } else if (peran === "Warewolf") {
    console.log(
      `Halo Warewolf ${nama}, Kamu akan memakan mangsa setiap malam!`
    );
  }
}

// Switch Case
var tanggal = 2; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = "06"; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1997; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var bulanString;
switch (Number(bulan)) {
  case 1:
    bulanString = "Januari";
    break;
  case 2:
    bulanString = "Februari";
    break;
  case 3:
    bulanString = "Maret";
    break;
  case 4:
    bulanString = "April";
    break;
  case 5:
    bulanString = "Mei";
    break;
  case 6:
    bulanString = "Juni";
    break;
  case 7:
    bulanString = "Juli";
    break;
  case 8:
    bulanString = "Agustus";
    break;
  case 9:
    bulanString = "September";
    break;
  case 10:
    bulanString = "Oktober";
    break;
  case 11:
    bulanString = "November";
    break;
  case 12:
    bulanString = "Desember";
    break;
  default:
    break;
}

var hasil = tanggal + " " + bulanString + " " + tahun;
console.log(hasil);
