// Code di sini
function range(startNum, endNum) {
    if (!startNum || !endNum) return -1;
    
    const result = [];

    if (startNum < endNum) {
        for (let i = startNum; i <= endNum; i++) {
            result.push(i);
        }
    } else {
        for (let i = startNum; i >= endNum; i--) {
            result.push(i);
        }
    }
    return result;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

function rangeWithStep(startNum, endNum, step) {
    if (!startNum || !endNum) return -1;
    
    const result = [];

    if (startNum < endNum) {
        for (let i = startNum; i <= endNum; i+=step ) {
            result.push(i);
        }
    } else {
        for (let i = startNum; i >= endNum; i-=step) {
            result.push(i);
        }
    }
    return result;
}

console.log()
console.log('==========================')
console.log()
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


function sum(startNum, endNum = startNum, step = 1) {
    // if (!startNum || !endNum) return -1;
    
    let result = 0;

    if (startNum < endNum) {
        for (let i = startNum; i <= endNum; i+=step ) {
            result += i
        }
    } else {
        for (let i = startNum; i >= endNum; i-=step) {
            result += i;
        }
    }
    return result;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log()
console.log('=================')
console.log()

function dataHandling(input) {
    for (let i = 0; i < input.length; i++) {
        let result = '';
        result += `Nomor ID: ${input[i][0]} \n`;
        result += `Nama Lengkap: ${input[i][1]} \n`;
        result += `TTL: ${input[i][2]} ${input[i][3]},\n`;
        result += `Hobi: ${input[i][4]}\n`;
        console.log(result);
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input);


function balikKata(kata) {
    let result = '';

    for (let i = kata.length - 1; i >= 0; i--) {
        result += kata[i];
    }
    return result;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


function dataHandling2(input) {
    input.splice(1, 5, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input);

    let tanggal = input[3];
    let tanggalSplit = tanggal.split('/');
    let bulan = tanggalSplit[1];
    let bulanString;
    switch (Number(bulan)) {
        case 1:
          bulanString = "Januari";
          break;
        case 2:
          bulanString = "Februari";
          break;
        case 3:
          bulanString = "Maret";
          break;
        case 4:
          bulanString = "April";
          break;
        case 5:
          bulanString = "Mei";
          break;
        case 6:
          bulanString = "Juni";
          break;
        case 7:
          bulanString = "Juli";
          break;
        case 8:
          bulanString = "Agustus";
          break;
        case 9:
          bulanString = "September";
          break;
        case 10:
          bulanString = "Oktober";
          break;
        case 11:
          bulanString = "November";
          break;
        case 12:
          bulanString = "Desember";
          break;
        default:
          break;
      }
    console.log(bulanString);

    console.log(tanggalSplit.sort((a, b) => b - a))

    console.log(tanggal.split('/').join('-'))

    let nama = input[1].slice(0, 14);
    console.log(nama);

}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);