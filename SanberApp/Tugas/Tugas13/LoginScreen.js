import React from 'react';
import { View, Image, Text, StyleSheet, TextInput, Button, TouchableOpacity } from 'react-native';
import logo from './images/logo.png'

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image source={logo} style={{ width: 375, height: 110 }} />
            </View>
            <View style={styles.formLogin}>
                <Text style={styles.title}>Login</Text>
                
                <View style={styles.formInput}>
                    <Text style={{ color: '#003366', marginBottom: 4, fontSize: 16 }} >Username/Email</Text>
                    <TextInput style={{ height: 40, borderColor: '#003366', borderWidth: 1 }} />
                </View>
                <View style={styles.formInput}>
                    <Text style={{ color: '#003366', marginBottom: 4, fontSize: 16 }} >Password</Text>
                    <TextInput style={{ height: 40, borderColor: '#003366', borderWidth: 1 }} />
                </View>

                <View style={styles.formButton}>
                    <TouchableOpacity>
                        <View style={[styles.btn, styles.btnLogin]}>
                            <Text style={styles.btnTitle}>Masuk</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={{ color: '#3EC6FF', fontSize: 24, marginVertical: 16 }} >atau</Text>
                    <TouchableOpacity>
                        <View style={[styles.btn, styles.btnRegister]}>
                            <Text style={styles.btnTitle}>Daftar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logoContainer: {
        alignItems: 'center',
        width: '100%',
        marginTop: 63,
    },
    formLogin: {
        alignItems: 'center',
        padding: 40
    },
    formInput: {
        width: '100%',
        marginTop: 16
    },
    title: {
        fontSize: 24,
        lineHeight: 28
    },
    formButton: {
        width: '100%',
        marginTop: 32,
        alignItems: 'center'
    },
    btn: {
        width: 140,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8
    },
    btnTitle: {
        color: 'white',
        fontSize: 24,
        justifyContent: 'center'
    },
    btnLogin: {
        backgroundColor: '#3EC6FF',
    },
    btnRegister: {
        backgroundColor: '#003366'
    },
})

export default LoginScreen;
